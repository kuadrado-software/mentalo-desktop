// Modules to control application life and create native browser window
const { app, BrowserWindow, globalShortcut } = require("electron");
const path = require("path");

const remote_main = require("@electron/remote/main");
const { images_path } = require("./src/constants");
remote_main.initialize();

function set_remote_web_contents(web_contents) {
    remote_main.enable(web_contents);
}


function createWindow() {
    const win = new BrowserWindow({
        width: 1400,
        height: 900,
        icon: path.join(images_path, "logo_mentalo_2.png"), // TODO use favicon.ico for Windows build, get path from os dependent constant
        backgroundColor: "rgb(51, 51, 51)",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            nativeWindowOpen: true,
        },
    });

    win.loadFile("index.html");
    win.maximize();
    set_remote_web_contents(win.webContents);

    return win;
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
    let win = createWindow();

    app.on("activate", function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) {
            win = createWindow();
        };
    });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
    if (process.platform !== "darwin") app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
