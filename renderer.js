"use strict";

const renderer = require("object-to-html-renderer");
const RootPage = require("./src/root-page");

renderer.register("obj2htm");
obj2htm.setRenderCycleRoot(new RootPage());
obj2htm.renderCycle();
