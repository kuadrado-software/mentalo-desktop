# Mentalo

**An app to build simple games with a simplle interface**

Mentalo is an application that allows to build a game project based on static screens and text choices. You can create your images inside the app, export you project in a file a import/edit it later.

Mentalo is a free and open source software released under a GNU-GPL-3.0 license.

---

## Structure of a game created with Mentalo

The game board is compound with a main space for the image, a space for the inventory in wich the player can store some objects picked along the game, and a space for the choices the player can click.

A game contains a set of scenes. Each scene has an image, animated or not. A scene can contain some objects, wich are little sprite images positionned on top of the scene image, the player can take those objects.

Each scene can have until 4 choices the player can click. A choice can lead to another scene under certain conditions, or it can enf the game.
Each scene can also contain a sound track.

A scene can be either playable, or cinematic. If a scene is a cinematic, it cannot have choices or objects to pick. It just has a minimum length and the behavior of the program at the end of the cinematic must be pre-defined (it can be go to another, or end the game).

The game also contains the metadata defined by the creator about interface appearance, fonts, panel colors, etc.

```
game
    |_Interface metadata: panels colors, fonts, proportions, etc...
    |_Game metadata:
    |   |_title
    |
    |_Scenes
    |   - A scene
    |       |_Title
    |       |_Image / animation
    |       |_mode: cinematic or playable
    |       |_Sound track
    |       |_objects
    |       |   - An object
    |       |       |_ image
    |       |       |_ position
    |       |   - Another object
    |       |   - ...
    |       |_ choices
    |       |   - A choice
    |       |       |_ Displayed clickable text
    |       |       |_ Next scene (or end of program)
    |       |       |_ conditions (objects required, error message if missing, etc)
    |       |   - Another choice
    |       |   - ...
    |       |
    |       (if scene is cinematic)
    |       |_End of cinematic automation
    |           |_ Duration
    |           |_ Next scene or end of program
    |   - Another scene
    |   - ...
    |
    |_Inventory: The objects picked by the player along the game.
```

---

# DEVELOPMENT

## Application structure

The app is run with [electronjs](https://www.electronjs.org/). 

The renderer process  (`./renderer.js`) runs a JavaScript components tree mapped to DOM with a tiny lib [object-to-html-renderer](https://www.npmjs.com/package/object-to-html-renderer);

## Local installation of the project

### Prerequisite

-   Node / NPM
-   git

### Init the project

```bash
git clone https://gitlab.com/kuadrado-software/mentalo-desktop.git
cd mentalo-desktop
npm install
```

### Run

```bash
npm run start
```

### Watch and build scss files

```bash
npm run watch-style
# or without watch
npm runn style
```
