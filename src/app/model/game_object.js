"use strict";

const { MtlGameObject } = require("mentalo-engine");

/**
 * The type for the fields game_objects of a Scene instance
 */
class GameObject extends MtlGameObject {
    /**
     * Initializes the state related to the manipulation of the object accross the interface,
     * for example to drag and drop it to a new position.
     */
    constructor(shared_state) {
        super(shared_state);
        this.name = "untitled game object";

        this.ui_state = {
            drag_drop_listeners: {
                mousedown: undefined,
                mousemove: undefined,
                mouseup: undefined,
            },
            cursor_listeners: {
                mousemove: undefined,
            },
            edit_listeners: {
                dblclick: undefined,
            }
        };
    }

    /**
     * Updates the position of the object with a {x, y} object
     * @param {Object} pos 
     */
    set_position(pos) {
        this.position = pos
    }

    /**
     * Removes the event listeners related to this game object.
     * @param {HTMLElement} listeners_target The reference of the canvas listening to the events
     */
    clear_listeners(listeners_target) {
        Object.keys(this.ui_state.drag_drop_listeners).forEach(etype => {
            const listener = this.ui_state.drag_drop_listeners[etype]
            if (listener) {
                listeners_target.removeEventListener(etype, listener);
            }
        });

        Object.keys(this.ui_state.cursor_listeners).forEach(etype => {
            const listener = this.ui_state.cursor_listeners[etype];
            if (listener) {
                listeners_target.removeEventListener(etype, listener);
            }
        });

        Object.keys(this.ui_state.edit_listeners).forEach(etype => {
            const listener = this.ui_state.cursor_listeners[etype];
            if (listener) {
                listeners_target.removeEventListener(etype, listener);
            }
        });

        this.ui_state.drag_drop_listeners = {
            mousedown: undefined,
            mousemove: undefined,
            mouseup: undefined,
        };

        this.ui_state.cursor_listeners.mousemove = undefined;
        this.ui_state.edit_listeners.dblclick = undefined;
    }

    /**
     * Removes the events= listeners related to the dropping of the object to a new position
     * @param {HTMLElement} target The canvas reference
     */
    clear_drop(target) {
        target.removeEventListener("mouseup", this.ui_state.drag_drop_listeners.mouseup);
        target.removeEventListener("mousemove", this.ui_state.drag_drop_listeners.mousemove);
        this.ui_state.drag_drop_listeners.mousemove = undefined;
        this.ui_state.drag_drop_listeners.mouseup = undefined;
    }

    /**
     * @returns {Object} The instance formatted as a litteral object
     */
    as_litteral() {
        return {
            name: this.name,
            image: this.image.name,
            position: this.position,
        }
    }

    /**
     * @returns {JSON} The instance serialized
     */
    serialized() {
        return JSON.stringify(this.as_litteral())
    }
}

module.exports = GameObject;