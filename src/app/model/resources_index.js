"use strict";

const MtlResourcesIndex = require("mentalo-engine/model/resources_index");

class ResourcesIndex extends MtlResourcesIndex {
    add_image(img_data) {
        const found_img_same_name = this.get_image(img_data.name);
        if (found_img_same_name) {
            this.images[this.images.indexOf(found_img_same_name)] = img_data;
        } else {
            this.images.push(img_data);
        }
    }

    add_sound(snd_data) {
        const found_snd_same_name = this.get_sound(snd_data.name);
        if (found_snd_same_name) {
            this.sounds[this.sounds.indexOf(found_snd_same_name)] = snd_data;
        } else {
            this.sounds.push(snd_data)
        }
    }

    set_resources(key, data) {
        this[key] = data;
    }


    delete_unused_resources(key, scenes, data_mode = "instance") {
        const keep_resources = [];
        switch (key) {
            case "images":
                this.images.forEach(im_res => {
                    const found_use = scenes
                        .find(s => s.animation.name === im_res.name)
                        || scenes
                            .map(s => s.game_objects)
                            .flat()
                            .find(gobj => {
                                const img_name = data_mode === "raw" ? gobj.image : gobj.image.name;
                                return img_name === im_res.name
                            });

                    found_use && keep_resources.push(im_res);
                });
                break;
            case "sounds":
                this.sounds.forEach(snd_res => {
                    const found_use = scenes.find(s => s.sound_track.name === snd_res.name);
                    found_use && keep_resources.push(snd_res);
                });
                break;
        }

        this.set_resources(key, keep_resources)
    }

    as_litteral() {
        return {
            images: this.images,
            sounds: this.sounds,
        }
    }
}

module.exports = ResourcesIndex