"use strict";

const { MtlSoundTrack } = require("mentalo-engine");

/**
 * The type used for the sound_track field of a Scene instance
 */
class SoundTrack extends MtlSoundTrack {
    /**
     * @returns {JSON} This instance serialized
     */
    serialized() {
        return JSON.stringify(this.as_litteral())
    }

    /**
     * @returns {Object} The sound track instance a litteral object
     */
    as_litteral() {
        return {
            name: this.name,
            _loop: this.audio.loop,
        }
    }
}

module.exports = SoundTrack;