"use strict";
const { SCENE_TYPES, color_tools } = require("mentalo-engine");

/**
 * The component that displays the inventory panel of the scene.
 */
class SceneInventory {
    /**
     * @param {Object} params Required params are project<Project> and on_edit_game_object<Function>
     */
    constructor(params) {
        this.params = params;
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const { project } = this.params;
        const base_style = project.game_ui_options.general;
        const canvas_height = base_style.animation_canvas_dimensions.height();
        const project_style = project.game_ui_options.inventory;
        const gap = project_style.gap;
        const h = canvas_height - (2 * project_style.padding);
        const gap_h = (project_style.rows - 1) * gap;
        const gap_w = (project_style.columns - 1) * gap
        const slot_side = (h - gap_h) / project_style.rows;
        const w = (project_style.columns * slot_side)
            + (2 * project_style.padding)
            + gap_w;

        const inventory_objects = Array.from(project.state.inventory);

        return {
            tag: "div",
            id: "inventory-container",
            class: "canvas-container",
            style_rules: {
                height: `${canvas_height}px`,
                width: `${w}px`,
            },
            contents: [
                {
                    tag: "div",
                    class: "canvas-like",
                    id: "inventory-canvas",
                    style_rules: {
                        backgroundColor: project_style.background_color,
                        padding: `${project_style.padding}px`,
                        display: "grid",
                        gap: `${gap}px`,
                        gridTemplateColumns: `repeat(${project_style.columns}, 1fr)`,
                        gridTemplateRows: `repeat(${project_style.rows}, 1fr)`,
                        visibility: this.params.project.get_scene()._type === SCENE_TYPES.PLAYABLE ? "visible" : "hidden",
                    },
                    contents: Array.from({ length: project_style.columns * project_style.rows }).map((_, i) => {
                        const draw_obj = inventory_objects[i];
                        const obj_w = draw_obj ? draw_obj.image.width : 1;
                        const obj_h = draw_obj ? draw_obj.image.height : 1;
                        const obj_ratio = obj_w / obj_h;

                        const border_color = color_tools
                            .get_optimal_visible_foreground_color(project_style.background_color);

                        return {
                            tag: "div", style_rules: {
                                width: `${slot_side}px`,
                                height: `${slot_side}px`,
                                border: `${project_style.slot_border_width}px solid ${border_color}`,
                                borderRadius: `${project_style.slot_rounded_corner_radius}px`,
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                overflow: "hidden"
                            },
                            contents: draw_obj ? [
                                {
                                    tag: "img", src: draw_obj.image.src,
                                    onclick: () => this.params.on_edit_game_object(draw_obj),
                                    style_rules: {
                                        cursor: "pointer",
                                        width: obj_ratio > 1 ? "100%" : obj_ratio < 1 ? "auto" : "100%",
                                        height: obj_ratio > 1 ? "auto" : obj_ratio < 1 ? "100%" : "auto",
                                    }
                                }
                            ] : undefined
                        }
                    }),
                }
            ]
        };
    }
}

module.exports = SceneInventory;