"use strict";

const ChoiceInput = require("../inputs/choice-input");
const GameObjectInput = require("../inputs/game-object-input");
const SceneSettingsInput = require("../inputs/scene-settings-input");
const TextBoxInput = require("../inputs/text-box-input");
const SceneImageInput = require("../inputs/scene-image-input/scene-image-input");
const SceneSoundtrackInput = require("../inputs/scene-soundtrack-input");

const GameObject = require("../../model/game_object");
const Choice = require("../../model/choice");
const SoundTrack = require("../../model/sound-track");
const Animation = require("../../model/animation");

const EditableText = require("../inputs/editable-text/editable-text");
const SceneImageCanvas = require("../scene-image-canvas");
const SceneChoices = require("../scene-choices");
const SceneInventory = require("../scene-inventory");
const { MentaloDrawingTool } = require("mentalo-drawing-tool");

const { MAX_WIDTH_SCENE, MAX_WIDTH_OBJECT } = require("mentalo-drawing-tool");
const { mentalo_drawing_tool_editable_file_mime, build_png_output } = require("mentalo-drawing-tool");
const { ui_config } = require("../../../constants");
const { SCENE_TYPES } = require("mentalo-engine");

const icon_objects = require("../../../jsvg/icon_objects");
const icon_play = require("../../../jsvg/icon_play");
const icon_dialog = require("../../../jsvg/icon_dialog");
const icon_delete = require("../../../jsvg/icon_delete");
const icon_film = require("../../../jsvg/icon_film");
const icon_cog = require("../../../jsvg/icon_cog");
const icon_music = require("../../../jsvg/icon_music");
const icon_add_choice = require("../../../jsvg/icon_add_choice");

const icon_book = require("../../../jsvg/icon_book");
const NotifPopup = require("../../../notif-popup");
const ChooseExistingImageInput = require("../inputs/choose-existing-image-input/choose-existing-image-input");
const ChooseExistingSoundtrackInput = require("../inputs/choose_existing_soundtrack_input/choose_existing_soundtrack_input");
const { create_documentation_window } = require("../../documentation");

/**
 * The containing component of the whole editor project view 
 */
class ProjectView {
    constructor(params) {
        this.params = params;

        // Wether the scene text box must be shown or not
        this.ui_state = {
            show_text_box: true,
        };

        // An index of the actions available in the toolbar on top of the scene view
        this.actions = [
            {
                tip: "Scene settings",
                onclick: this.handle_set_scene_settings.bind(this),
                icon: icon_cog,
            },
            {
                tip: "Set the image or animation",
                icon: icon_film,
                onclick: this.handle_set_scene_animation.bind(this)
            },
            {
                tip: "Play the animation",
                icon: icon_play,
                onclick: () => this.params.project.get_scene().animation.reset_frame(),
                cond: () => this.params.project.get_scene()._type === SCENE_TYPES.CINEMATIC
            },
            {
                tip: "Add a text box",
                icon: icon_dialog,
                onclick: this.handle_add_text_box.bind(this),
                cond: () => this.params.project.get_scene()._type === SCENE_TYPES.PLAYABLE && this.params.project.get_scene().text_box === ""
            },
            {
                tip: "Edit the text box",
                icon: icon_dialog,
                onclick: this.handle_add_text_box.bind(this),
                cond: () => this.params.project.get_scene()._type === SCENE_TYPES.PLAYABLE && this.params.project.get_scene().text_box !== ""
            },
            {
                tip: "Add an object to the scene",
                icon: icon_objects,
                onclick: this.handle_add_game_object.bind(this),
                cond: () => this.params.project.get_scene()._type === SCENE_TYPES.PLAYABLE
            },
            {
                tip: "Add sound track to the scene",
                icon: icon_music,
                onclick: this.handle_set_scene_soundtrack.bind(this)
            },
            {
                tip: "Add a choice",
                icon: icon_add_choice,
                onclick: this.handle_add_choice.bind(this),
                cond: () => this.params.project.get_scene()._type === SCENE_TYPES.PLAYABLE
                    && this.params.project.get_scene().choices.length < this.params.project.game_ui_options.choices_panel.max_choices,
            },
            {
                tip: "Delete the scene",
                icon: icon_delete,
                onclick: this.handle_delete_scene.bind(this),
            },
        ];
    }

    /**
     * Refreshes the rendering from this component. 
     * If a dialbox is open it will be cleaned and closed.
     */
    refresh() {
        this.params.kill_dialbox();
        obj2htm.subRender(this.render(), document.getElementById("project-view"), { mode: "replace" });
    }

    /**
     * Refreshed the rendering of the scene view
     */
    refresh_scene_image() {
        obj2htm.subRender(this.render_scene_image(), document.getElementById("scene-image-container"), { mode: "replace" })
    }

    /**
     * Handles the click on the "delete scene" action button.
     * Opens a confirmation dialog box.
     */
    handle_delete_scene() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Delete the scene?"),
            render_object: {
                tag: "div", contents: [
                    {
                        tag: "p", contents: edit_scene.name,
                        style_rules: {
                            textAlign: "center",
                            backgroundColor: "#0003",
                            margin: "10px 0",
                            padding: "20px",
                        }
                    },
                    {
                        tag: "p", class: "warn-message", contents: t("Images, objects and settings of this scene will be deleted.")
                    }
                ]
            },
            on_cancel: hide_dialbox,
            on_submit: () => {
                project.delete_scene(edit_scene);
                set_saved(false);
                this.refresh();
                this.params.refresh_action_panel();
            }
        });
    }

    /**
     * Shows the scene text box preview on top of the scene image.
     */
    handle_show_text_box() {
        this.ui_state.show_text_box = !this.ui_state.show_text_box;
        this.refresh_scene_image();
    }

    /**
     * Handle the click on the "Add a text box" action.
     * Opens a dialog box holding the form to set the scene text box.
     */
    handle_add_text_box() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene()

        show_dialbox({
            title: t("Display a message in the scene"),
            render_object: new TextBoxInput({ edit_scene }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const text = document.getElementById("scene-text-box-input-textarea").value;
                edit_scene.text_box = text;
                this.ui_state.show_text_box = true;
                set_saved(false);
                this.refresh();
            },
            add_actions: edit_scene.text_box !== "" ? [
                {
                    text: t("Delete the text box"),
                    icon: { ...icon_delete },
                    onclick: () => {
                        edit_scene.text_box = "";
                        this.ui_state.show_text_box = false;
                        set_saved(false);
                        this.refresh();
                    }
                }
            ] : undefined
        });
    }

    /**
     * Handles the click on the "Add a choice" action.
     * Opens the dialog box with a form to set up a new choice.
     */
    handle_add_choice() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Add a new choice to the scene"),
            render_object: new ChoiceInput({ project, scene: edit_scene }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const new_choice = new Choice(document.getElementById("choice-input-value").get_value());
                edit_scene.add_choice(new_choice);
                set_saved(false);
                this.refresh();
            },
        });
    }

    /**
     * Handle the click on an existing choice.
     * Opens a dialog box with a form to edit the current setup of the choice.
     * @param {Choice} choice 
     */
    handle_edit_choice(choice) {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Modify the choice"),
            render_object: new ChoiceInput({
                project,
                scene: edit_scene,
                edit_choice: choice,
            }).render(),
            add_actions: [
                {
                    text: t("Delete that choice"),
                    icon: { ...icon_delete },
                    onclick: () => {
                        edit_scene.remove_choice(choice);
                        hide_dialbox();
                        this.refresh();
                    }
                }
            ],
            on_cancel: hide_dialbox,
            on_submit: () => {
                const updated = new Choice(document.getElementById("choice-input-value").get_value());
                edit_scene.update_choice(choice, updated);
                set_saved(false);
                this.refresh();
            },
        });
    }

    /**
     * Handles the click on the "Scene settings" action.
     * Opens a dial box with the form to set up the properties of the scene, 
     * the scene type, and the options for te cinematic mode.
     */
    handle_set_scene_settings() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Properties of the scene"),
            render_object: new SceneSettingsInput({
                scene: edit_scene, project,
            }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const input = document.getElementById("select-scene-type-input");
                edit_scene.set_type(input.value);

                if (input.value === SCENE_TYPES.CINEMATIC) {
                    const duration_input = document.getElementById("scene-cinematic-duration-input");
                    const destination_scene_input = document.getElementById("cinematic-destination-scene-input");
                    const end_cinematic_option_quit_input = document.getElementById("end-cinematic-action-input-quit");

                    edit_scene.cinematic_duration = parseInt(duration_input.value);

                    edit_scene.end_cinematic_options = {
                        destination_scene_index: destination_scene_input ? parseInt(destination_scene_input.value) : -1,
                        quit: end_cinematic_option_quit_input.checked
                    };
                }

                set_saved(false);
                this.refresh();
            },
        });
    }

    /**
     * Checks that a game object name is not already taken
     * @param {String} name 
     * @returns {Boolean} true if the name is available
     */
    validate_game_object_name(name, edit_object = undefined) {
        const { project } = this.params;

        const valid = !project.scenes
            .map(s => s.game_objects)
            .flat()
            .filter(obj => obj !== edit_object)
            .some(obj => obj.name === name);

        if (!valid) {
            (new NotifPopup({ error: true, message: t("Another object already has the same name") })).pop();
        }

        return valid;
    }

    /**
     * Handles the click on the "Add an object" action.
     * Opens a dialbox with a form to set up a new game object.
     */
    handle_add_game_object() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Add a new object to the scene"),
            render_object: new GameObjectInput({
                project,
                on_create_image: this.handle_create_image.bind(this, { mode: "object" }),
                on_choose_image_from_existing: this.get_image_resources("object").length > 0
                    ? this.handle_choose_image_from_existing.bind(this, { mode: "object" })
                    : undefined,
            }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const input_file = document.getElementById("game-object-file-input");

                const error = {
                    isset: false,
                    message: "",
                };

                if (!input_file.files || typeof FileReader !== "function") {
                    error.isset = true;
                    error.message = t("Error : Your browser doesn't support file reading :(");
                } else if (input_file.files && !input_file.files[0]) {
                    error.isset = true;
                    error.message = t("No file selected");
                }

                if (error.isset) {
                    hide_dialbox();
                    (new NotifPopup({ error: true, message: error.message })).pop();
                    return;
                } else {
                    const input_name = document.getElementById("game-object-name-input");
                    const img_file = input_file.files[0];
                    const drawing_tool_editable = img_file.name.includes(`.${mentalo_drawing_tool_editable_file_mime}`);
                    const file_reader = new FileReader();
                    file_reader.onload = drawing_tool_editable ? () => {
                        const data = JSON.parse(file_reader.result);
                        const src = build_png_output({
                            width: data.image_width,
                            height: data.image_height,
                            frames: data.frames_pixel_data.slice(0, 1).map(fr => {
                                const img_data = new ImageData(data.image_width, data.image_height);
                                img_data.data.set(fr);
                                return img_data;
                            }),
                            animation: false,
                        });

                        if (this.validate_game_object_name(input_name.value)) {
                            edit_scene.add_game_object(this.create_game_object({
                                image_name: img_file.name,
                                image: src,
                                name: input_name.value,
                            }));

                            set_saved(false);
                            this.refresh();
                        }

                    } : () => {
                        if (this.validate_game_object_name(input_name.value)) {
                            edit_scene.add_game_object(this.create_game_object({
                                image_name: img_file.name,
                                image: file_reader.result,
                                name: input_name.value,
                            }));

                            set_saved(false);
                            this.refresh();
                        }

                    };

                    drawing_tool_editable ? file_reader.readAsText(img_file) : file_reader.readAsDataURL(img_file);
                }
            }
        });
    }

    /**
     * Handles the click on an existing game object (on its image located on the scene image).
     * Opens a dial box with a form to edit the current data
     * @param {GameObject} gobj 
     */
    handle_edit_game_object(gobj) {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const inventory_objects = project.state.inventory;
        show_dialbox({
            title: t("Modify the object"),
            render_object: new GameObjectInput({
                edit_object: gobj,
                on_create_image: this.handle_create_image.bind(this, {
                    mode: "object",
                    edit_object: gobj,
                }),
                on_edit_image: this.handle_create_image.bind(this, {
                    mode: "object",
                    edit_object: gobj,
                    edit_image: {
                        src: gobj.image.src,
                        dimensions: gobj.get_dimensions(),
                    },
                }),
                on_choose_image_from_existing: this.get_image_resources("object").length > 0
                    ? this.handle_choose_image_from_existing.bind(this, {
                        mode: "object",
                        edit_object: gobj,
                    })
                    : undefined,
            }).render(),
            add_actions: [
                {
                    text: t("Preview in the inventory"),
                    onclick: this.handle_show_object_in_inventory.bind(this, gobj),
                    cond: () => project.inventory_has_empty_slot() && !inventory_objects.has(gobj)
                },
                {
                    text: t("Hide the inventory preview"),
                    onclick: this.handle_remove_object_from_inventory.bind(this, gobj),
                    cond: () => inventory_objects.has(gobj),
                },
                {
                    text: t("Remove this object from the scene"),
                    icon: icon_delete,
                    onclick: this.handle_delete_game_object.bind(this, gobj)
                }
            ],
            on_cancel: hide_dialbox,
            on_submit: () => {
                const input_file = document.getElementById("game-object-file-input");
                const input_name = document.getElementById("game-object-name-input");
                const img_file = input_file.files[0];

                if (img_file) {
                    const drawing_tool_editable = img_file.name.includes(`.${mentalo_drawing_tool_editable_file_mime}`);
                    const file_reader = new FileReader();
                    file_reader.onload = drawing_tool_editable ? () => {
                        const data = JSON.parse(file_reader.result);
                        const src = build_png_output({
                            width: data.image_width,
                            height: data.image_height,
                            frames: data.frames_pixel_data.slice(0, 1).map(fr => {
                                const img_data = new ImageData(data.image_width, data.image_height);
                                img_data.data.set(fr);
                                return img_data;
                            }),
                            animation: false,
                        });



                        if (this.validate_game_object_name(input_name.value, gobj)) {
                            project.resources_index.add_image({
                                name: img_file.name,
                                src,
                            });

                            gobj.image.name = img_file.name;
                            gobj.image.src = src;
                            gobj.name = input_name.value;

                            set_saved(false);
                            this.refresh();
                        }
                    } : () => {
                        if (this.validate_game_object_name(input_name.value, gobj)) {

                            project.resources_index.add_image({
                                name: img_file.name,
                                src: file_reader.result,
                            });

                            gobj.image.name = img_file.name;
                            gobj.image.src = file_reader.result;
                            gobj.name = input_name.value;

                            set_saved(false);
                            this.refresh();
                        }
                    };

                    drawing_tool_editable
                        ? file_reader.readAsText(img_file)
                        : file_reader.readAsDataURL(img_file);

                } else {
                    if (this.validate_game_object_name(input_name.value, gobj)) {
                        gobj.name = input_name.value;
                        set_saved(false);
                        this.refresh();
                    }
                }
            }
        });
    }

    /**
     * Handles the click on the delete button from the edit game object form.
     * @param {Gameobject} gobj 
     */
    handle_delete_game_object(gobj) {
        const { project, set_saved } = this.params;
        const edit_scene = project.get_scene();
        edit_scene.remove_game_object(gobj);

        if (project.state.inventory.has(gobj)) {
            project.remove_object_from_inventory(gobj);
        }

        set_saved(false);
        this.refresh();
    }

    /**
     * Creates a GameObject instance from litteral data.
     * This is called from the form when adding a new object to the scene.
     * @param {Object} data 
     * @returns {GameObject}
     */
    create_game_object(data) {
        const { image, image_name, name } = data;
        const { project } = this.params;
        const game_object = new GameObject({ resources_index: project.resources_index });

        project.resources_index.add_image({
            name: image_name,
            src: image,
        });

        game_object.load_data({
            image: image_name,
            name,
            position: { x: 0, y: 0 },
        });

        return game_object;
    }

    /**
     * Get the available image resources for scenes or objects
     * @param {String} mode scene or object
     * @returns {[Object]} A collection of image resources 
     */
    get_image_resources(mode) {
        const { project } = this.params;

        return project.resources_index.images.filter(im_res => {
            return mode === "scene"
                ? !!im_res.frame_nb
                : !im_res.frame_nb;
        });
    }

    /**
     * Handles the click on the "Scene animation" action.
     * Opens a dial box with form to upload or create an image and define it as the animation of the edited scene.
     */
    handle_set_scene_animation() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Set up the image of the scene"),
            render_object: new SceneImageInput({
                current_anim: edit_scene.animation,
                on_create_image: this.handle_create_image.bind(this, { mode: "scene" }),
                on_edit_image: this.handle_create_image.bind(this, {
                    mode: "scene",
                    edit_image: edit_scene.animation
                }),
                on_choose_image_from_existing: this.get_image_resources("scene").length > 0
                    ? this.handle_choose_image_from_existing.bind(this, { mode: "scene" })
                    : undefined,
            }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const input = document.getElementById("scene-image-file-input");

                const error = {
                    isset: false,
                    message: "",
                };

                if (input.files && !input.files[0] && !edit_scene.animation.initialized) {
                    error.isset = true;
                    error.message = t("No file selected");
                } else if (!input.files || typeof FileReader !== "function") {
                    error.isset = true;
                    error.message = t("Error : Your browser doesn't support file reading :(");
                }

                if (error.isset) {
                    hide_dialbox();
                    (new NotifPopup({ error: true, message: error.message })).pop();
                    return;
                } else {
                    const input_name = document.getElementById("scene-image-name-input");
                    const input_framenb = document.getElementById("scene-anim-frame-nb-input");
                    const input_speed = document.getElementById("scene-anim-speed-input");
                    const input_loop = document.getElementById("scene-anim-loop-input");

                    const name = input_name.value;
                    const frame_nb = parseInt(input_framenb.value);
                    const speed = input_speed ? parseInt(input_speed.value) : 1;

                    const play_once = input_loop
                        ? !input_loop.checked
                        : edit_scene._type === SCENE_TYPES.CINEMATIC
                            ? true
                            : false;

                    if (input.files[0]) {
                        const img_file = input.files[0];
                        const drawing_tool_editable = img_file.name.includes(`.${mentalo_drawing_tool_editable_file_mime}`);
                        const file_reader = new FileReader();
                        file_reader.onload = drawing_tool_editable
                            ? () => {
                                const data = JSON.parse(file_reader.result);
                                const output = build_png_output({
                                    width: data.image_width,
                                    height: data.image_height,
                                    frames: data.frames_pixel_data.map(fr => {
                                        const img_data = new ImageData(data.image_width, data.image_height);
                                        img_data.data.set(fr);
                                        return img_data;
                                    }),
                                    animation: data.animation.on,
                                });

                                project.resources_index.add_image({
                                    src: output,
                                    name,
                                    frame_nb: data.frames_pixel_data.length
                                })

                                const anim = new Animation({ resources_index: project.resources_index });

                                anim.init({
                                    name,
                                    speed: data.animation.frequency,
                                    play_once: false,
                                });

                                edit_scene.animation = anim;

                                set_saved(false);
                                this.refresh();
                            }
                            : () => {

                                project.resources_index.add_image({
                                    src: file_reader.result,
                                    name,
                                    frame_nb,
                                });

                                const anim = new Animation({ resources_index: project.resources_index });

                                anim.init({
                                    name,
                                    speed,
                                    play_once,
                                });

                                edit_scene.animation = anim;

                                set_saved(false);
                                this.refresh();
                            };

                        drawing_tool_editable ?
                            file_reader.readAsText(img_file)
                            : file_reader.readAsDataURL(img_file);

                    } else {
                        const current_anim = edit_scene.animation;

                        project.resources_index.update_image(current_anim.name, { name, frame_nb });

                        current_anim.update_data({
                            name, speed, play_once,
                        });

                        set_saved(false);
                        this.refresh();
                    }
                }
            }
        });
    }

    /**
     * Generates an automatic game object name.
     * @returns {String}
     */
    generate_game_object_name() {
        return `game_object_${this.get_image_resources("object").length + 1}`
    }

    /**
     * Handles the click on the Create image button from the "scene animation" form or the "add object" form,
     * which are the 2 possible elements using an image.
     * Opens a MentaloDrawingTool instance and defines the callbacks to use the output image on close the drawing applet.
     * @param {Object} args
     */
    handle_create_image(args = {}) {
        const { show_dialbox, project, set_saved, config } = this.params;
        const { edit_image, edit_object, mode } = args;

        if (edit_image && edit_image.dimensions.width > (
            mode === "scene"
                ? MAX_WIDTH_SCENE
                : MAX_WIDTH_OBJECT)
        ) {
            (new NotifPopup({ error: true, message: t("Image is too large to be edited with that tool.") })).pop();
        } else {
            const format_edit_image = edit_image
                ? (function () {
                    const out = {
                        src: "",
                        dimensions: {
                            width: 0,
                            height: 0,
                        },
                        frame_nb: 1,
                        frequency: 1,
                    }
                    switch (mode) {
                        case "scene":
                            out.src = edit_image.image.src;
                            out.dimensions = edit_image.dimensions;
                            out.frame_nb = edit_image.frame_nb;
                            out.frequency = edit_image.speed;
                            break;
                        case "object":
                            out.src = edit_image.src;
                            out.dimensions = edit_image.dimensions;
                            break;
                    }

                    return out;
                })()
                : undefined;

            show_dialbox({
                title: "",
                style_rules: {
                    padding: 0,
                    display: "flex",
                    alignItems: "center",
                    position: "absolute",
                    width: "100vw",
                    top: 0, left: 0, right: 0, bottom: 0,
                    zIndex: 100,
                },
                render_object: new MentaloDrawingTool({
                    prefered_language: config.get_entry("prefered_language"),
                    canvas_ratio: project.game_ui_options.general.animation_canvas_dimensions.ratio,
                    canvas_width: mode === "object" ? 400 : 800,
                    on_exit_callback: created_image_data => {
                        const { output, frame_nb, animation_frequency, name } = created_image_data;

                        if (output) {
                            switch (mode) {
                                case "scene":
                                    const new_anim = new Animation({ resources_index: project.resources_index });

                                    project.resources_index.add_image({
                                        name,
                                        src: output,
                                        frame_nb,
                                    });

                                    new_anim.init({
                                        name,
                                        speed: animation_frequency,
                                        play_once: false,
                                    });

                                    project.get_scene().animation = new_anim;

                                    break;
                                case "object":
                                    if (edit_object) {
                                        edit_object.image.src = output;
                                        edit_object.image.name = name;
                                        project.resources_index.add_image({
                                            name,
                                            src: output
                                        });
                                    } else {
                                        project
                                            .get_scene()
                                            .add_game_object(
                                                this.create_game_object({
                                                    image: output,
                                                    image_name: name, // name is the name of the mentalo-drawing-app output, so it's the image_name
                                                    name: this.generate_game_object_name(),
                                                })
                                            );
                                    }

                                    break;
                            }
                            set_saved(false);
                        }

                        this.refresh();
                    },
                    mode,
                    edit_image: format_edit_image,
                    renderer: obj2htm,
                }).render(),
                prevent_cancel: true,
            });
        }
    }

    handle_delete_unused_resources(key) {
        const { project } = this.params;

        project.resources_index.delete_unused_resources(key, project.scenes);
    }

    handle_choose_image_from_existing(options) {
        const { mode, edit_object } = options;
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Choose the image to be reused"),
            render_object: new ChooseExistingImageInput({
                edit_scene,
                get_images: this.get_image_resources.bind(this, mode),
                delete_unused_images: this.handle_delete_unused_resources.bind(this, "images")
            }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const value = document.getElementById("choose-existing-image-input-value").value;

                if (mode === "scene") {
                    const anim = new Animation({ resources_index: project.resources_index });

                    // Find the other scene that uses that same resource and get its settings as default for this one.
                    const default_settings = {
                        speed: 1,
                        play_once: false,
                    };

                    const { speed, play_once } = mode === "scene" ? (() => {
                        const using_scene = project.scenes.find(s => s.animation.name === value);
                        return using_scene ? using_scene.animation : default_settings;
                    })() : default_settings;

                    anim.init({
                        name: value,
                        speed,
                        play_once,
                    });

                    edit_scene.animation = anim;
                } else {
                    const img_res = project.resources_index.get_image(value);

                    if (edit_object) { // object mode, existing object
                        edit_object.image.src = img_res.src;
                        edit_object.image.name = img_res.name;

                    } else { // object mode, new object
                        project
                            .get_scene()
                            .add_game_object(this.create_game_object({
                                name: this.generate_game_object_name(),
                                image_name: img_res.name,
                                image: img_res.src,
                            }));
                    }
                }

                set_saved(false);
                this.refresh();
            }
        })
    }

    handle_choose_soundtrack_from_existing() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Choose the soundtrack to be reused"),
            render_object: new ChooseExistingSoundtrackInput({
                get_sounds: () => project.resources_index.sounds,
                delete_unused_soundtracks: this.handle_delete_unused_resources.bind(this, "sounds")
            }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const value = document.getElementById("choose-existing-soundtrack-input-value").value;

                const scene_using = project.scenes
                    .filter(s => s !== edit_scene)
                    .find(s => s.sound_track.name === value);

                const snd_track = new SoundTrack({ resources_index: project.resources_index });

                snd_track.init({
                    name: value,
                    _loop: scene_using.sound_track.audio.loop,
                });

                edit_scene.sound_track = snd_track;

                set_saved(false);
                this.refresh();
            }
        })
    }

    /**
     * Handles the click on the "Scene soundtrack" action.
     * Opens a dialbox with a form to upload a soundtrack a set it as the soundtrack of the edited scene.
     */
    handle_set_scene_soundtrack() {
        const { show_dialbox, hide_dialbox, project, set_saved } = this.params;
        const edit_scene = project.get_scene();

        show_dialbox({
            title: t("Set up the sound track of the scene"),
            render_object: new SceneSoundtrackInput({
                current_track: edit_scene.sound_track,
                resources_index: project.resources_index,
                on_choose_soundtrack_from_existing: project.resources_index.sounds.length > 0
                    ? this.handle_choose_soundtrack_from_existing.bind(this)
                    : undefined
            }).render(),
            on_cancel: hide_dialbox,
            on_submit: () => {
                const input = document.getElementById("scene-soundtrack-file-input");

                const error = {
                    isset: false,
                    message: "",
                };

                if (input.files && !input.files[0] && !edit_scene.sound_track.initialized) {
                    error.isset = true;
                    error.message = t("No file selected");
                } else if (!input.files || typeof FileReader !== "function") {
                    error.isset = true;
                    error.message = t("Error : Your browser doesn't support file reading :(");
                }

                if (error.isset) {
                    hide_dialbox();
                    (new NotifPopup({ error: true, message: error.message })).pop();
                    return;
                } else {
                    const name = document.getElementById("scene-soundtrack-name-input").value;
                    const _loop = document.getElementById("scene-soundtrack-loop-input").checked;

                    if (input.files[0]) {
                        const snd_file = input.files[0];
                        const file_reader = new FileReader();

                        file_reader.onload = () => {
                            project.resources_index.add_sound({
                                name,
                                src: file_reader.result,
                            });

                            const snd_track = new SoundTrack({ resources_index: project.resources_index });

                            snd_track.init({
                                name,
                                _loop
                            });

                            edit_scene.sound_track = snd_track;
                        };

                        file_reader.readAsDataURL(snd_file);

                    } else {
                        const current_track = edit_scene.sound_track;
                        project.resources_index.update_sound(current_track.name, { name });
                        current_track.name = name;
                        current_track.audio.loop = _loop;
                    }

                    set_saved(false);
                    this.refresh();
                }
            },
            add_actions: [
                {
                    text: t("Delete the track"),
                    icon: icon_delete,
                    onclick: () => {
                        this.handle_delete_soundtrack();
                        set_saved(false);
                        this.refresh();
                    },
                    cond: () => edit_scene.sound_track.initialized,
                }
            ]
        });
    }

    /**
     * Handles the click on the delete button from the "scene soundtrack" form.
     */
    handle_delete_soundtrack() {
        const edit_scene = this.params.project.get_scene();
        edit_scene.sound_track.reset();
        this.params.set_saved(false);
    }

    /**
     * Handles the click on the "Preview object in inventory" button from the "edit game object" form.
     * @param {GameObject} game_object 
     */
    handle_show_object_in_inventory(game_object) {
        this.params.project.add_object_to_inventory(game_object);
        this.refresh();
    }

    /**
     * Handle the click on the "Hide game object from inventory" button from the "edit game object" form.
     * @param {GameObject} game_object 
     */
    handle_remove_object_from_inventory(game_object) {
        this.params.project.remove_object_from_inventory(game_object);
        this.refresh();
    }

    /**
     * @returns {Object} The object representation of the html to render for the project title component.
     */
    render_project_title() {
        const { project, set_saved } = this.params;
        return new EditableText({
            text: project.name,
            tag: "h1",
            id: "project-title",
            tooltip: t("Click to modify the title of the project"),
            on_edit_submit: txt => {
                project.name = txt;
                set_saved(false);
                obj2htm.subRender(this.render_project_title(), document.getElementById("project-title"), { mode: "replace" })
            }
        }).render();
    }

    /**
     * @returns {Object} The object representation of the html to render for the scene title component.
     */
    render_scene_title() {
        const edit_scene = this.params.project.get_scene();
        return new EditableText({
            text: edit_scene.name,
            tag: "h2",
            id: "edit-scene-title",
            tooltip: t("Click to modify the title of this scene"),
            on_edit_submit: txt => {
                if (this.params.project.scenes.find(s => s !== edit_scene && s.name === txt)) {
                    (new NotifPopup({ error: true, message: t("Another scene already has this title") })).pop();
                } else {
                    edit_scene.name = txt;
                    this.params.set_saved(false);
                    obj2htm.subRender(this.render_scene_title(), document.getElementById("edit-scene-title"), { mode: "replace" });
                    this.params.refresh_action_panel();
                }
            }
        }).render();
    }

    /**
     * @returns {Object} The object representation of the html to render for the image of the scene.
     */
    render_scene_image() {
        const project_style = this.params.project.game_ui_options;

        return {
            tag: "div",
            id: "scene-image-container",
            class: "canvas-container",
            style_rules: {
                height: `${project_style.general.animation_canvas_dimensions.height()}px`,
                width: `${project_style.general.animation_canvas_dimensions.width}px`,
            },
            contents: [
                new SceneImageCanvas({
                    project: this.params.project,
                    ui_options: {
                        show_text_box: this.ui_state.show_text_box,
                        on_close_text_box: this.handle_show_text_box.bind(this),
                        on_text_box_dblclick: this.handle_add_text_box.bind(this),
                        on_game_object_dblclick: this.handle_edit_game_object.bind(this),
                    }
                }).render()
            ],
        }
    }

    /**
     * @returns {Object} The object representation of the html to render for the choices panel below the scene image.
     */
    render_scene_choices() {
        const { project } = this.params;

        return new SceneChoices({
            project,
            on_edit_choice: this.handle_edit_choice.bind(this),
            on_add_choice: this.handle_add_choice.bind(this)
        }).render()
    }

    /**
     * @returns {Object} The object representation of the html to render for the inventory panel at the right of the scene image.
     */
    render_scene_inventory() {
        return new SceneInventory({
            project: this.params.project,
            on_edit_game_object: this.handle_edit_game_object.bind(this)
        }).render()
    }

    /**
     * @returns {Object} The object representation of the html to render for this component.
     */
    render() {
        const { actions } = this;
        const game_style = this.params.project.game_ui_options.general;
        const game_render_width = `${(() => {
            const canvas_height = game_style.animation_canvas_dimensions.height();
            const project_style = this.params.project.game_ui_options.inventory;
            const gap = project_style.gap;
            const h = canvas_height - (2 * project_style.padding);
            const gap_h = (project_style.rows - 1) * gap;
            const gap_w = (project_style.columns - 1) * gap
            const slot_side = (h - gap_h) / project_style.rows;
            const inventory_w = (project_style.columns * slot_side)
                + (2 * project_style.padding)
                + gap_w;
            return inventory_w + game_style.animation_canvas_dimensions.width + (ui_config.project_view.padding * 2)
        })()}px`;

        return {
            tag: "div",
            id: "project-view",
            contents: [
                {
                    tag: "div", id: "project-scene-title", contents: [
                        this.render_project_title(),
                        this.render_scene_title(),
                    ]
                },
                {
                    tag: "div",
                    id: "edit-scene-actions",
                    style_rules: { width: game_render_width },
                    class: "view-board-toolbox",
                    contents: [
                        {
                            tag: "ul",
                            contents: actions.filter(action => action.cond ? action.cond() : true).map(action => {
                                return {
                                    tag: "li",
                                    contents: [
                                        {
                                            tag: "button",
                                            class: "action-button",
                                            tooltip: t(action.tip),
                                            contents: [{ ...action.icon }],
                                            onclick: action.onclick
                                        }
                                    ]
                                }
                            }).concat([
                                {
                                    tag: "li",
                                    style_rules: { marginLeft: "auto" },
                                    contents: [
                                        {
                                            tag: "button",
                                            class: "action-button",
                                            tooltip: t("Open the documentation"),
                                            contents: [{ ...icon_book }],
                                            onclick: create_documentation_window,
                                        }
                                    ]
                                }
                            ])
                        }
                    ]
                },
                {
                    tag: "div",
                    id: "game-render-container",
                    contents: [
                        {
                            tag: "div",
                            id: "game-render-view",
                            style_rules: {
                                backgroundColor: game_style.background_color,
                                width: game_render_width,
                                padding: `${ui_config.project_view.padding}px`,
                            },
                            contents: [
                                this.render_scene_image(),
                                this.render_scene_inventory(),
                                this.render_scene_choices(),
                            ]
                        },
                    ]
                }
            ]
        }
    }
}

module.exports = ProjectView;