"use strict";


/**
 * The component that displays the settings form for the inventory panel in the side toolbar
 */
class InventorySettingsForm {
    constructor(params) {
        this.params = params;
    }

    /**
     * Handles changes from an input for any property
     * @param {String} property the property name
     * @param {Evenet} e 
     */
    handle_change_property(property, e) {
        const value = !isNaN(parseInt(e.target.value)) ? parseInt(e.target.value) : e.target.value;
        this.params.project.game_ui_options.inventory[property] = value;
        this.params.on_change_setting();
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const current_settings = this.params.project.game_ui_options.inventory;
        return {
            tag: "ul", class: "ui-settings-form",
            contents: [
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Panel") }
                            ]
                        },
                        {
                            tag: "ul", class: "settings-list", contents: [
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Columns") },
                                        {
                                            tag: "input", type: "number", min: 1, step: 1, value: current_settings.columns,
                                            onchange: e => {
                                                let value = parseInt(e.target.value);
                                                e.target.value = value < 1 ? 1 : value;
                                                this.handle_change_property("columns", e)
                                            },
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Rows") },
                                        {
                                            tag: "input", type: "number", min: 3, step: 1, value: current_settings.rows,
                                            onchange: e => {
                                                let value = parseInt(e.target.value);
                                                e.target.value = value < 3 ? 3 : value;
                                                this.handle_change_property("rows", e)
                                            },
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Background color") },
                                        {
                                            tag: "input", type: "color", value: current_settings.background_color,
                                            onchange: this.handle_change_property.bind(this, "background_color")
                                        }
                                    ]
                                },
                            ]
                        }
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("Slots") }
                            ]
                        },
                        {
                            tag: "ul", class: "settings-list", contents: [
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Spacing") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 50, step: 1, value: current_settings.gap,
                                            onchange: this.handle_change_property.bind(this, "gap")
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Rounded corners") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 80, step: 1,
                                            value: current_settings.slot_rounded_corner_radius,
                                            onchange: this.handle_change_property.bind(this, "slot_rounded_corner_radius")
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Borders width") },
                                        {
                                            tag: "input", type: "range", min: 0, max: 10, step: 1,
                                            value: current_settings.slot_border_width,
                                            onchange: this.handle_change_property.bind(this, "slot_border_width")
                                        }
                                    ]
                                },
                            ]
                        }
                    ]
                },
            ]
        }
    }
}

module.exports = InventorySettingsForm;