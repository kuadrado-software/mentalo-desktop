"use strict";

const icon_cancel = require("../../../jsvg/icon_cancel");
const icon_check = require("../../../jsvg/icon_check");

/**
 * A generic dialog box component.
 * The content of the box is defined by the params.render_object parameter.
 */
class DialBox {
    /**
     * Sets the contents to render and attaches the event listeners to close the dialbox.
     * Throws an error if params.render_object is not defined
     * @param {Object} params 
     */
    constructor(params) {
        if (!params.render_object) {
            throw new Error("Dialbox render object is not set");
        }

        this.params = params;

        this.id = performance.now() + "dial-box";

        this.cancel_listeners = {
            click_outside: {
                type: "click",
                f: e => {
                    if (e.target.id === "dial-box-modal" && !this.params.prevent_cancel) {
                        this.cancel();
                    }
                }
            },
            press_escape: {
                type: "keydown",
                f: e => {
                    if (e.key === "Escape" && !this.params.prevent_cancel) {
                        this.cancel();
                    }
                }
            }
        };

        Object.values(this.cancel_listeners).forEach(listener => {
            window.addEventListener(listener.type, listener.f);
        });

        this.input_error = false;
    }

    /**
     * Handles the cancelling of the action being built with the dialbox.
     */
    cancel() {
        this.remove_cancel_listeners();
        if (this.input_error) this.clear_errors();
        this.params.on_cancel();
    }

    /**
     * Handles the submitting of the action built with the dialbox.
     */
    submit() {
        if (this.input_error) this.clear_errors();

        const proceed = this.check_required_inputs();
        if (proceed) {
            this.remove_cancel_listeners();
            this.params.on_submit();
        }
    }

    /**
     * Resets the error state of the dialbox
     */
    clear_errors() {
        const err_field = document.querySelector(".required-field-err-msg")
        err_field && err_field.remove();

        const invalid_field = document.getElementById(this.id)
            .querySelector(".input-required-invalid");

        if (invalid_field) {
            invalid_field.classList.remove("input-required-invalid");
            invalid_field.removeEventListener("input", this.tmp_input_listener);
        }

        this.input_error = false;
    }

    /**
     * Attaches an event listener to an input whose value have been parsed as invalid.
     */
    listen_input_after_error() {
        this.tmp_input_listener = this.clear_errors.bind(this);
        document.getElementById(this.id)
            .querySelector(".input-required-invalid")
            .addEventListener("input", this.tmp_input_listener);
    }

    /**
     * Search for any input in the dialbox content with the attribute "required" set and check if the value is valid
     * @returns {Boolean} true if no invalid entry has been found
     */
    check_required_inputs() {
        const required_inputs = document.getElementById(this.id).querySelectorAll(':required');
        for (const input of required_inputs) {
            if (!input.checkValidity()) {
                input.classList.add("input-required-invalid");

                const err_msg_elmt = document.createElement("div");
                err_msg_elmt.innerHTML = t("This field is required");
                err_msg_elmt.classList.add("required-field-err-msg");

                const input_bounds = input.style.display !== "none"
                    ? input.getBoundingClientRect()
                    : input.parentNode.getBoundingClientRect();
                err_msg_elmt.style.top = parseInt(input_bounds.bottom + 5).toString() + "px";
                err_msg_elmt.style.left = parseInt(input_bounds.left).toString() + "px";
                document.body.appendChild(err_msg_elmt);

                this.listen_input_after_error();
                this.input_error = true;
                return false;
            }
        }
        return true;
    }

    /**
     * Removes the event listeners that handles the cancelling of the dialbox action.
     */
    remove_cancel_listeners() {
        Object.values(this.cancel_listeners).forEach(listener => {
            window.removeEventListener(listener.type, listener.f);
        });
    }

    /**
     * Refreshes the rendering tree from this component
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" })
    }

    /**
     * 
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        return {
            tag: "div",
            id: this.id,
            class: "dial-box",
            style_rules: { ...this.params.style_rules },
            contents: [
                this.params.title && {
                    tag: "div", class: "dial-box-title", contents: [
                        { tag: "h3", contents: this.params.title },
                    ]
                },
                this.params.render_object,
                this.params.on_submit && {
                    tag: "div",
                    class: "action-buttons-row align-end",
                    contents: (this.params.add_actions
                        ? this.params.add_actions
                            .filter(action => action.cond ? action.cond() : true)
                            .map(action => {
                                return {
                                    tag: "button",
                                    class: `${action.icon ? "action-button" : "action-text-button"}`,
                                    tooltip: action.icon ? action.text : "",
                                    contents: action.icon ? [{ ...action.icon }] : action.text,
                                    onclick: action.onclick,
                                }
                            })
                        : []).concat([
                            {
                                tag: "button",
                                class: `${this.params.submit_button_text && !this.params.submit_button_icon ? "action-text-button" : "action-button"}`,
                                tooltip: this.params.submit_button_text && !this.params.submit_button_icon
                                    ? ""
                                    : this.params.submit_button_text || t("Validate"),
                                contents: this.params.submit_button_icon
                                    ? [{ ...this.params.submit_button_icon }]
                                    : this.params.submit_button_text || [{ ...icon_check }],
                                onclick: this.submit.bind(this)
                            },
                            {
                                tag: "button",
                                class: `${this.params.cancel_button_text ? "action-text-button" : "action-button"}`,
                                tooltip: this.params.cancel_button_text ? "" : t("Cancel"),
                                contents: this.params.cancel_button_text || [{ ...icon_cancel }],
                                onclick: this.cancel.bind(this)
                            }
                        ])
                }
            ]
        }
    }
}

module.exports = DialBox;