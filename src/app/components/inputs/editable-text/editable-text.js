"use strict";

/**
 * A generic component to display a text that can be edited as a text input when clicking on it.
 */
class EditableText {
    /**
     * @param {Object} params 
     * Params must contain a on_edit_submit callback and a tag<String> entry 
     * that defines the type of html element the text must be displayed in.
     */
    constructor(params) {
        this.params = params;
        this.edit = false;
        this.id = this.params.id;
        this.text_id = this.params.id + "-" + this.params.tag;
        this.input_id = this.params.id + "-input";
        this.form_id = this.params.id + "form";
    }

    /**
     * Handles the submitting of the input, clears the listeners and call on_edit_submit callback parameter
     * @param {Event} e 
     */
    handle_submit_edit(e) {
        e.preventDefault();
        this.remove_cancel_edit_listeners();
        const text = e.target.getElementsByTagName("input")[0].value;
        this.params.on_edit_submit(text);
    }

    /**
     * Toggles the display state of the component in edit mode and refreshes the rendering.
     * The text will be displayed as an <input type="text" />.
     * Attaches event listeners to the input to handle the cancelling.
     */
    handle_show_edit_form() {
        this.edit = true;

        this.append_input_listener = e => {
            if (e.detail.outputNode.id === this.form_id) {
                document.getElementById(this.input_id).focus();
                window.removeEventListener(obj2htm.event_name, this.append_input_listener);
            }
        };

        window.addEventListener(obj2htm.event_name, this.append_input_listener);

        this.cancel_listeners = {
            click_outside: {
                type: "click",
                callback: e => {
                    if (e.target.id !== this.input_id && e.target.id !== this.text_id) {
                        this.handle_cancel_edit();
                    }
                }
            },
            escape: {
                type: "keydown",
                callback: e => {
                    if (e.key === "Escape") {
                        this.handle_cancel_edit();
                    }
                }
            }
        };

        Object.values(this.cancel_listeners).forEach(listener => {
            window.addEventListener(listener.type, listener.callback);
        });

        obj2htm.subRender(this.render_edit_form(), document.getElementById(this.id), { mode: "override" })

    }

    /**
     * Removes the event listeners that are attached when the rendering state is switched into edit mode.
     */
    remove_cancel_edit_listeners() {
        Object.values(this.cancel_listeners).forEach(listener => {
            window.removeEventListener(listener.type, listener.callback);
        });
    }

    /**
     * Handles the cancelling of the edit mode.
     */
    handle_cancel_edit() {
        this.edit = false;
        this.remove_cancel_edit_listeners();
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
    }

    /**
     * @returns {Object} The object representation of the html to render for the text input when the component is in edit mode.
     */
    render_edit_form() {
        return {
            tag: "form",
            id: this.form_id,
            onsubmit: e => this.handle_submit_edit(e),
            contents: [
                {
                    tag: "input",
                    id: this.input_id,
                    type: "text",
                    value: this.params.text,
                    class: "editable-text-input"
                }
            ]
        }
    }

    /**
     * @returns {Object} The object representation of the html to render for this component.
     */
    render() {
        return {
            tag: "div",
            id: this.id,
            class: `editable-text editable-${this.params.tag}`,
            contents: [
                this.edit ? this.render_edit_form() : {
                    tag: this.params.tag,
                    id: this.text_id,
                    tooltip: this.params.tooltip,
                    contents: this.params.text,
                    onclick: this.handle_show_edit_form.bind(this)
                }
            ]
        }
    }
}

module.exports = EditableText;