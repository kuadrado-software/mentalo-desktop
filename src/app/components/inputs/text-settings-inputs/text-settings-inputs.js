"use strict";
const { font_tools } = require("mentalo-engine");
const {
    FONT_FAMILIES,
    FONT_WEIGHT_OPTIONS,
    TEXT_ALIGN_OPTIONS
} = font_tools;

/**
 * A generic form component that displayed a set of settings for text styling (font family, bold, align etc)
 */
class TextSettingsInputs {
    /**
     * @param {Object} params 
     * Required params are on_change_property<Function(property_key<String>, property_value<Any>)> and current_settings<Object>
     */
    constructor(params) {
        this.params = params;
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        const { on_change_property, current_settings } = this.params;

        return {
            tag: "ul", class: "text-settings-inputs settings-list", contents: [
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "text-setting-block",
                            tooltip: t("Font family"),
                            contents: [
                                {
                                    tag: "label", contents: [
                                        { tag: "span", contents: "F", style_rules: { fontFamily: "cursive", fontSize: "18px" } },
                                        { tag: "span", contents: "f", style_rules: { fontFamily: "Courier, monospace", fontSize: "18px" } },
                                    ]
                                },
                                {
                                    tag: "select",
                                    onchange: e => on_change_property("font_family", e.target.value),
                                    contents: FONT_FAMILIES.map(group => {
                                        return {
                                            tag: "optgroup",
                                            label: group.category,
                                            contents: group.values.map(font => {
                                                return {
                                                    tag: "option",
                                                    value: font.value,
                                                    style_rules: {
                                                        // this is not working on most browsers ... Should create a custom select input
                                                        fontFamily: font.value,
                                                    },
                                                    contents: font.text,
                                                    selected: font.value === current_settings.font_family
                                                };
                                            }),
                                        };
                                    })
                                }
                            ]
                        },
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "button", class: "text-setting-button " + (current_settings.font_style === "italic" ? "active" : ""),
                            tooltip: t("Italic"),
                            contents: "<i>i</i>", onclick: () => on_change_property(
                                "font_style",
                                current_settings.font_style === "italic" ? "normal" : "italic"
                            ),
                        }
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "text-setting-buttons-group", contents: FONT_WEIGHT_OPTIONS.map(opt => {
                                return {
                                    tag: "button", class: "text-setting-button " + (current_settings.font_weight.toString() === opt.value ? "active" : ""),
                                    contents: "a",
                                    tooltip: t(opt.text),
                                    style_rules: {
                                        fontWeight: opt.value,
                                    },
                                    onclick: () => on_change_property("font_weight", opt.value),
                                }
                            })
                        }
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "text-setting-block", tooltip: t("Font size"), contents: [
                                {
                                    tag: "label", contents: [
                                        { tag: "span", style_rules: { fontSize: "16px" }, contents: "T" },
                                        { tag: "span", style_rules: { fontSize: "10px" }, contents: "T" },
                                    ]
                                },
                                {
                                    tag: "input", type: "number", min: 8, max: 50, step: 1,
                                    value: current_settings.font_size,
                                    onchange: e => on_change_property("font_size", e.target.value),
                                }
                            ],
                        }
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "text-setting-block", tooltip: t("Text color"), contents: [
                                {
                                    tag: "label", contents: [
                                        { tag: "span", contents: "A", style_rules: { color: "rgb(40, 199, 61)" } },
                                        { tag: "span", contents: "b", style_rules: { color: "rgb(245, 211, 19)" } },
                                    ], style_rules: { fontSize: "16px" }
                                },
                                {
                                    tag: "input", type: "color", value: current_settings.font_color,
                                    onchange: e => on_change_property("font_color", e.target.value)
                                }
                            ]
                        }
                    ]
                },
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "text-setting-buttons-group", contents: TEXT_ALIGN_OPTIONS.map(opt => {
                                return {
                                    tag: "button", class: "text-setting-button " + (current_settings.text_align === opt.value ? "active" : ""),
                                    tooltip: `${t("Text align")} - ${t(opt.text).toLowerCase()}`,
                                    onclick: () => on_change_property("text_align", opt.value),
                                    contents: [
                                        {
                                            tag: "div", class: "text-align-symbol symbol-" + opt.value,
                                            contents: [100, 70, 55, 90].map(n => {
                                                return {
                                                    tag: "div", style_rules: {
                                                        border: "1px solid",
                                                        width: `${n}%`,
                                                        flex: 1,
                                                    }
                                                }
                                            })
                                        }]
                                }
                            })
                        }
                    ]
                }
            ]
        }
    }
}

module.exports = TextSettingsInputs;