"use strict";

/**
 * The component rendered as the content of the dialbox opened for the "Add text box" action
 */
class TextBoxInput {
    /**
     * @param {Object} params Param edit_scene<Scene> is required
     */
    constructor(params) {
        this.params = params;
        this.textarea_maxlength = 300;
        this.warn_maxlength = this.params.edit_scene.text_box.length > this.textarea_maxlength;
        this.textarea_value = this.params.edit_scene.text_box
        this.autofocus_listener = e => {
            if (e.detail.outputNode.querySelector("#scene-text-box-input-textarea")) {
                const el = document.getElementById("scene-text-box-input-textarea");
                el.focus();
                el.setSelectionRange && el.setSelectionRange(el.value.length, el.value.lenth);
                window.removeEventListener(obj2htm.event_name, this.autofocus_listener)
            }
        }
        window.addEventListener(obj2htm.event_name, this.autofocus_listener)
    }

    /**
     * @returns {Object} object representation of the html to render for the textarea input
     */
    render_textarea() {
        return {
            tag: "textarea",
            id: "scene-text-box-input-textarea",
            contents: this.textarea_value,
            rows: 5,
            cols: 50,
            oninput: e => {
                this.textarea_value = e.target.value;
                if (e.target.value.length > this.textarea_maxlength) {
                    this.warn_maxlength = true;
                    this.refresh_warn()
                } else {
                    if (this.warn_maxlength) {
                        this.warn_maxlength = false;
                        this.refresh_warn()
                    }
                }
            }
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the little warning displayed if the text is too long
     */
    render_warn() {
        return this.warn_maxlength ? {
            tag: "div", id: "scene-textbox-warn-max", contents: [
                {
                    tag: "span",
                    style_rules: { color: "red" },
                    contents: `${t("Max character number reached")}: ${this.textarea_value.length} / ${this.textarea_maxlength}`,
                }
            ]
        } : { tag: "div", id: "scene-textbox-warn-max" };
    }


    /**
     * Refreshes the warning rendering
     */
    refresh_warn() {
        obj2htm.subRender(this.render_warn(), document.getElementById("scene-textbox-warn-max"), { mode: "replace" });
    }

    /**
     * @returns {Object} The object representation of the html to render for this component
     */
    render() {
        return {
            tag: "div", id: "scene-text-box-input", contents: [
                this.render_textarea(),
                this.render_warn(),
            ]
        }
    }
}

module.exports = TextBoxInput;