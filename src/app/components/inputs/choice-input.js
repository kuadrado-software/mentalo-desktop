"use strict";

const Choice = require("../../model/choice");

/**
 * The component being displayed as the content of the dialbox opened for the "Add choice" or "Edit choice" actions.
 */
class ChoiceInput {
    constructor(params) {
        this.params = params;
        this.choice = params.edit_choice || new Choice();
        this.id = "scene-choice-input";
        this.output = { ...this.choice };

        this.autofocus_listener = e => {
            if (e.detail.outputNode.querySelector("#choice-input-text-field")) {
                const el = document.getElementById("choice-input-text-field");
                el.focus();
                el.setSelectionRange && el.setSelectionRange(el.value.length, el.value.length);
                window.removeEventListener(obj2htm.event_name, this.autofocus_listener);
            }
        }
        window.addEventListener(obj2htm.event_name, this.autofocus_listener);
    }

    /**
     * Handles the text typing event in the text input
     * @param {Event} e 
     */
    handle_choice_text_oninput(e) {
        const { output } = this;

        const text = e.target.value;
        output.text = text;
    }

    /**
     * Handle change from the select input for destination scene
     * @param {Event} e 
     */
    handle_change_destination_scene(e) {
        const { output } = this;

        const value = e.target.value;
        if (value !== "") {
            output.destination_scene_index = parseInt(value);
        }
    }

    /**
     * Handles change from checkbox, checked if choice requires to use an object
     * @param {Event} e 
     */
    handle_change_use_object(e) {
        this.output.use_objects.value = e.target.checked;
        this.refresh();
    }

    /**
     * Handle changes from multiselect input.
     * Sets the required objects for that choice.
     * @param {Event} e 
     */
    handle_select_required_objects(e) {
        const { output } = this;

        const values = Array.from(e.target.options).filter(op => op.selected).map(op => {
            const find_ob = output.use_objects.items.find(o => o.name === op.value);
            return {
                name: op.value,
                consume_object: find_ob ? find_ob.consume_object : false,
            }
        });
        output.use_objects.items = values;
        this.refresh();
    }

    /**
     * Handles checkbox input change and sets if the game object passed as params 
     * must be removed from inventory after the choice is clicked.
     * @param {GameObject} obj
     * @param {Event} e 
     */
    handle_change_consume_object(obj, e) {
        const { output } = this;
        const find_ob = output.use_objects.items.find(o => obj.name === o.name);
        find_ob.consume_object = e.target.checked;
        this.refresh();
    }

    /**
     * Handles the text typing in the text input for the message displayed if 
     * the choice is clicked but some required objects are missing.
     * @param {Event} e 
     */
    handle_change_missing_required_object_message(e) {
        const { output } = this;
        const text = e.target.value;
        output.use_objects.missing_object_message = text;
    }

    /**
     * Refreshes the rendering from this component
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" })
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const { output } = this;
        const { project } = this.params;
        const { scenes } = project;
        const select_scenes = scenes.filter(scene => scene !== project.get_scene());
        return {
            tag: "div",
            id: this.id,
            contents: [
                {
                    tag: "ul",
                    class: "list-settings",
                    contents: [
                        {
                            tag: "li",
                            tooltip: t("Explain shortly to the player the action you are proposing."),
                            contents: [
                                {
                                    tag: "label", contents: t("Text to display"), class: "center-y",
                                    style_rules: { width: "200px" },
                                },
                                {
                                    tag: "input", type: "text",
                                    style_rules: { flex: 1 },
                                    class: "center-y",
                                    id: "choice-input-text-field",
                                    value: output.text,
                                    oninput: this.handle_choice_text_oninput.bind(this),
                                    required: true,
                                },
                            ]
                        },
                        {
                            tag: "li",
                            tooltip: t("Choose the scene where the player will be directed to after clicking that choice. You can also choose to exit the game."),
                            contents: [
                                {
                                    tag: "label", contents: t("Destination"), class: "center-y",
                                    style_rules: { width: "200px" },
                                },
                                {
                                    tag: "select",
                                    style_rules: { flex: 1 },
                                    onchange: this.handle_change_destination_scene.bind(this),
                                    contents: [{
                                        tag: "option",
                                        value: "",
                                        contents: "-- " + t("Browse scenes") + " --"
                                    }]
                                        .concat(select_scenes.length === 0 ? [
                                            {
                                                tag: "option",
                                                value: "",
                                                disabled: true,
                                                contents: t("There is no scenes available. Create some and set up this later."),
                                                class: "empty-select-placeholder"
                                            }
                                        ] : select_scenes.map(scene => {
                                            const scene_index = scenes.indexOf(scene);
                                            return {
                                                tag: "option",
                                                value: scene_index,
                                                contents: scene.name,
                                                selected: output.destination_scene_index === scene_index,
                                            }
                                        })).concat([
                                            {
                                                tag: "option",
                                                value: -2,
                                                contents: t("Quit the game"),
                                                title: t("This will make the program exit if the player clicks the choice."),
                                                selected: output.destination_scene_index === -2,
                                            }
                                        ]),
                                },
                            ]
                        },
                        {
                            tag: "li",
                            tooltip: t("Select some objects the player must have in the inventory to be able to make that choice."),
                            contents: [
                                {
                                    tag: "label", contents: t("Require objects"),
                                    class: "center-y", style_rules: { width: "200px" },
                                    htmlFor: "mtlo-choice-input-require-objects-checkbox",
                                    style_rules: { cursor: "pointer" }
                                },
                                {
                                    tag: "input",
                                    type: "checkbox",
                                    id: "mtlo-choice-input-require-objects-checkbox",
                                    onchange: this.handle_change_use_object.bind(this),
                                    checked: output.use_objects.value
                                },
                            ]
                        },
                    ].concat(output.use_objects.value ? [
                        {
                            tag: "li",
                            style_rules: {
                                display: "grid",
                                gridTemplateColumns: "1fr 1.2fr",
                            },
                            contents: [
                                {
                                    tag: "select",
                                    style_rules: { height: "140px" },
                                    required: true,
                                    multiple: true,
                                    tooltip: t("Select the required objects. Press the control key to select more than one."),
                                    onchange: this.handle_select_required_objects.bind(this),
                                    contents: [
                                        {
                                            tag: "optgroup", label: t("Select the objects required to do that choice"),
                                            contents: (project.get_game_objects().length === 0 ? [{
                                                tag: "option",
                                                contents: t("There isn't any object in your game yet"),
                                                class: "empty-select-placeholder",
                                                disabled: true,
                                            }] : []).concat(project.get_game_objects().map(o => {
                                                return {
                                                    tag: "option", value: o.name, contents: o.name,
                                                    selected: !!output.use_objects.items.find(it => it.name === o.name)
                                                }
                                            }))
                                        },
                                    ],
                                },
                                output.use_objects.items.length > 0 ? {
                                    tag: "div",
                                    style_rules: {
                                        backgroundColor: "#0002",
                                        display: "flex",
                                        flexDirection: "column",
                                    },
                                    contents: [
                                        {
                                            tag: "label", contents: t("Selected objects options"),
                                            style_rules: { padding: "10px", backgroundColor: "#0002" }
                                        },
                                        {
                                            tag: "ul",
                                            style_rules: {
                                                overflowY: "scroll",
                                                height: "100px",
                                                listStyleType: "none",
                                                padding: "5px 10px",
                                                margin: 0,
                                            },
                                            contents: output.use_objects.items.map(it => {
                                                return {
                                                    tag: "li",
                                                    style_rules: {
                                                        display: "flex",
                                                        justifyContent: "space-between",
                                                        gap: "20px",
                                                        alignItems: "center"
                                                    },
                                                    contents: [
                                                        {
                                                            tag: "span", contents: it.name,
                                                            style_rules: {
                                                                width: "140px",
                                                                overflow: "hidden",
                                                                textOverflow: "ellipsis",
                                                                whiteSpace: "nowrap",
                                                            },
                                                        },
                                                        {
                                                            tag: "div",
                                                            tooltip: t("Remove the object from the inventory when the player clicks the choice."),
                                                            style_rules: {
                                                                display: "flex",
                                                                gap: "5px",
                                                                alignItems: "center"
                                                            },
                                                            contents: [
                                                                {
                                                                    tag: "input", type: "checkbox",
                                                                    id: `${it.name}-consume-inventory-checkbox`,
                                                                    onchange: this.handle_change_consume_object.bind(this, it),
                                                                    checked: (function () {
                                                                        const find_ob = output.use_objects.items.find(o => it.name === o.name);
                                                                        return find_ob && find_ob.consume_object;
                                                                    })()
                                                                },
                                                                {
                                                                    tag: "label", contents: t("Consume in inventory"),
                                                                    htmlFor: `${it.name}-consume-inventory-checkbox`,
                                                                    style_rules: { cursor: "pointer" }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            })
                                        },
                                    ]
                                } : { tag: "div", contents: " " },
                            ]
                        },
                        {
                            tag: "li",
                            tooltip: t("This message will be poped up to the player if he/she doesn't have the required objects in the inventory"),
                            contents: [
                                {
                                    tag: "label", contents: t("Missing object message"),
                                    class: "center-y", style_rules: { width: "200px" },
                                },
                                {
                                    tag: "input", type: "text", value: output.use_objects.missing_object_message,
                                    style_rules: { flex: 1 },
                                    oninput: this.handle_change_missing_required_object_message.bind(this),
                                    required: true,
                                }
                            ]
                        }
                    ] : [])
                },
                {
                    tag: "input", type: "hidden", id: "choice-input-value", get_value: () => output
                }
            ]
        }
    }
}

module.exports = ChoiceInput