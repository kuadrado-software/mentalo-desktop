"use strict";

const icon_trash_recycle = require("../../../../jsvg/icon_trash_recycle");


class ChooseExistingSoundtrackInput {
    constructor(params) {
        this.params = params;
        this.sounds = this.params.get_sounds();
        this.sounds.forEach((snd, i) => {
            snd.selected = i === 0
        });

        this.state = {
            list_scroll: 0,
        }
    }

    handle_select_sound(snd) {
        let value = "";
        this.sounds.forEach(snd_res => {
            snd_res.selected = snd_res === snd;
            if (snd_res.selected) {
                value = snd_res.name;
            }
        });

        document.getElementById("choose-existing-soundtrack-input-value").value = value;

        this.refresh_list();
    }

    refresh_sounds() {
        this.sounds = this.params.get_sounds();
    }

    handle_delete_unused_soundtracks() {
        this.params.delete_unused_soundtracks();
        this.refresh_sounds();
        this.refresh_list();
    }

    handle_scroll_list(e) {
        this.state.list_scroll = e.target.scrollTop;
    }

    handle_fix_list_scroll_top(list_node) {
        list_node.scrollTo(0, this.state.list_scroll);
    }

    refresh_list() {
        obj2htm.subRender(
            this.render_list(),
            document.querySelector("#choose-existing-soundtrack-input ul"),
            { mode: "replace" },
        );
    }

    render_list() {
        return {
            tag: "ul",
            on_render: this.handle_fix_list_scroll_top.bind(this),
            onscroll: this.handle_scroll_list.bind(this),
            contents: this.sounds.map(snd => {
                return {
                    tag: "li",
                    contents: snd.name,
                    style_rules: {
                        border: snd.selected ? "2px solid rgb(40, 199, 61)" : "2px solid transparent",
                        opacity: snd.selected ? 1 : .6,

                    },
                    onclick: this.handle_select_sound.bind(this, snd),
                }
            })
        }
    }

    render() {
        return {
            tag: "div",
            id: "choose-existing-soundtrack-input",
            contents: [
                {
                    tag: "input",
                    type: "hidden",
                    id: "choose-existing-soundtrack-input-value",
                    value: this.sounds.find(snd => snd.selected).name
                },
                this.render_list(),
                {
                    tag: "button",
                    class: "action-button",
                    tooltip: t("Delete unused soundtracks"),
                    contents: [{ ...icon_trash_recycle }],
                    onclick: this.handle_delete_unused_soundtracks.bind(this)
                },
            ]
        }
    }
}

module.exports = ChooseExistingSoundtrackInput;