"use strict";
const { BrowserWindow } = require("@electron/remote");
const path = require("path");
const { images_path } = require("../../constants");

function create_documentation_window() {
    const parent = BrowserWindow.getAllWindows()[0];
    const w = new BrowserWindow({
        parent,
        width: 1500,
        height: 900,
        icon: path.join(images_path, "logo_mentalo_2.png"), // TODO use favicon.ico for Windows build, get path from os dependent constant
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            nativeWindowOpen: true,
        },
    });

    w.loadFile(path.join(__dirname, "index.html"));
    w.removeMenu();
}

module.exports = {
    create_documentation_window,
};