"use strict";

const renderer = require("object-to-html-renderer");
const DocumentationPage = require("./documentation-page");

renderer.register("obj2htm");
obj2htm.setRenderCycleRoot(new DocumentationPage());
obj2htm.renderCycle();
