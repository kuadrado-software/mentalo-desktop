"use strict";

const LoaderModal = require("../loader-modal");
const ActionPanel = require("./components/action-panel/action-panel");
const DialBox = require("./components/dial-box/dial-box");
const ProjectView = require("./components/project-view/project-view");
const Project = require("./model/project");
const { Menu, app, dialog, process } = require("@electron/remote");
const icon_export = require("../jsvg/icon_export");
const icon_exit = require("../jsvg/icon_exit");
const { custom_project_mime, demo_game_assets_path } = require("../constants");
const NotifPopup = require("../notif-popup");
const { file_has_ext } = require("../utils");
const Config = require("../config");
const Translator = require("../translator");
const fs = require("fs");
const os = require("os");
const path = require("path");
const { create_documentation_window } = require("./documentation");

/**
 * The Mentalo core application, to create and edit games.
 */
class App {
    constructor(params) {
        this.params = params;
        this.id = "app-screen";
        this.config = new Config();
        this.translator = new Translator(this.config);
        this.project = params.project || new Project();
        this.generate_menu();

        this.state = params.app_state || {
            saved: true,
            save_project_path: "",
        };
    }

    handle_open_with_file() {
        // packaged mode only (use 3rd arg in dev mode)
        if (process.argv.length >= 2) {
            const f_path = process.argv[1];
            this.load_from_path(f_path);
        }
    }

    generate_menu() {
        const { recently_opened_files } = this.config.data;
        const recently_opened_items = recently_opened_files.length > 0 ?
            recently_opened_files
                .filter(f => fs.existsSync(f))
                .map(f => {
                    return {
                        label: f.replace(os.homedir(), "~"),
                        click: this.load_from_path.bind(this, f)
                    }
                }) :
            [{
                label: t("No recent files"),
                enabled: false,
            }];

        this.menu = Menu.buildFromTemplate([
            {
                label: t("File"),
                submenu: [
                    {
                        label: t("New"),
                        accelerator: 'CommandOrControl+n',
                        click: this.create_new_project.bind(this),
                    },
                    {
                        label: t("Open"),
                        submenu: (recently_opened_items).concat([
                            { type: "separator" },
                            {
                                label: t("Choose"),
                                role: "open",
                                accelerator: 'CommandOrControl+o',
                                click: this.open_project.bind(this),
                            },
                        ])
                    },
                    {
                        label: t("Save"),
                        click: this.save_project.bind(this),
                        accelerator: "CommandOrControl+s",
                    },
                    {
                        label: t("Save as"),
                        click: this.save_project.bind(this, { save_as: true }),
                        accelerator: "CommandOrControl+Shift+s",
                    },
                    {
                        label: t("Quit"),
                        click: this.on_exit_project.bind(this),
                        accelerator: "CommandOrControl+q",
                    },
                ]
            },
            {
                label: t("Preferences"),
                submenu: [
                    {
                        label: t("Application language"),
                        submenu: [
                            {
                                label: t("English"),
                                click: this.change_application_language.bind(this, "en"),
                            },
                            {
                                label: t("French"),
                                click: this.change_application_language.bind(this, "fr"),
                            },
                            {
                                label: t("Spanish"),
                                click: this.change_application_language.bind(this, "es"),
                            },
                        ]
                    }
                ]
            },
            {
                label: t("View"),
                submenu: [
                    {
                        label: t("Zoom in"),
                        role: "zoomIn",
                        accelerator: "CommandOrControl+numadd"
                    },
                    {
                        label: t("Zoom out"),
                        role: "zoomOut",
                        accelerator: "CommandOrControl+numsub"
                    },
                    {
                        label: t("Reset zoom"),
                        role: "resetZoom",
                        accelerator: "CommandOrControl+num0"
                    }
                ]
            },
            {
                label: t("Help"),
                submenu: [
                    {
                        label: t("Documentation"),
                        click: create_documentation_window,
                    },
                    {
                        label: t("Open demo game"),
                        click: this.load_from_path.bind(this, path.join(demo_game_assets_path, "Home-Sweet-Home.mtl"))
                    }
                ]
            }
        ]);

        Menu.setApplicationMenu(this.menu);

    }

    change_application_language(value) {
        this.translator.change_locale(value);
        this.config.set_entry("prefered_language", value);
        this.generate_menu();
        this.refresh();
    }

    create_new_project(options = { ignore_unsaved: false, save_and_exit: false }) {
        if (this.state.saved || options.ignore_unsaved) {
            this.project = new Project();
            this.set_saved(true);
            this.refresh(true);
        } else if (options.save_and_exit) {
            this.save_project({ on_saved: this.create_new_project.bind(this) });
        } else {
            this.on_exit_unsaved_project(this.create_new_project.bind(this));
        }
    }

    /**
     * Open a file from menu command
     */
    open_project() {
        dialog.showOpenDialog(null, {
            properties: ['openFile'],
            filters: [
                {
                    name: "all",
                    extensions: ["mtl"]
                }
            ]
        }).then(file_obj => {
            if (!file_obj.canceled) {
                const f_path = file_obj.filePaths[0];
                this.load_from_path(f_path);
            }
        }).catch(err => console.log(err));
    }

    load_from_path(f_path) {
        if (!fs.existsSync(f_path)) {
            (new NotifPopup({
                message: t("File not found"),
                error: true,
            })).pop();
            this.config.remove_recently_open_file(f_path);
            this.generate_menu();
        } else if (this.validate_file_mime({ name: path.basename(f_path) })) {
            this.show_loading_state({ message: t("Project is loading") + "..." });

            const data = JSON.parse(fs.readFileSync(f_path));
            this.config.add_recently_opened_file(f_path);
            this.generate_menu();
            this.hide_loading_state();
            this.on_load_project_data(data);
        }
    }

    /**
     * Will be called as callback when project is saved to a file
     * @param {Object} options 
     */
    save_project(options = { exit: false, load_data: undefined, save_as: false }) {
        this.write_project_file(!!options.save_as);

        if (options.exit) {
            this.on_exit_project();
        } else if (options.load_data) {
            this.on_load_project_data(options.load_data);
        } else if (options.on_saved) {
            options.on_saved();
        }
    }

    update_title_unsaved_tip() {
        const title = document.querySelector("title").innerHTML.replace("⚙️ ", "");

        document.querySelector("title").innerHTML = !this.state.saved
            ? "⚙️ " + title
            : title.replace("⚙️ ", "");
    }

    get_default_save_path() {
        return path.join(os.homedir(), this.project.name.replaceAll(" ", "-") + "." + custom_project_mime);
    }

    write_project_file(save_as = false) {
        const project_json = this.project.serialized();

        if (save_as || !this.save_path) {
            this.save_path = dialog.showSaveDialogSync(null, {
                defaultPath: this.save_path || this.get_default_save_path()
            });

            if (!this.save_path) return;
        }

        fs.writeFileSync(this.save_path, project_json);

        if (fs.existsSync(this.save_path)) {
            this.set_saved(true);

            (new NotifPopup({
                message: t("Project saved!")
            })).pop();
        }
    }

    /**
     * 
     * @param {Object} file An object with a name property representing the file name including mime
     * @returns 
     */
    validate_file_mime(file) {
        if (!file_has_ext(file, custom_project_mime)) {
            (new NotifPopup({
                error: true,
                message: t(
                    "Please select a .*** file",
                    { ext: custom_project_mime })
            })).pop();
            return false;
        }
        return true;
    }


    /**
     * This is called from the parent component (App) and handles the warning if project has unsaved changes
     * @param {Function} callback 
     */
    on_exit_unsaved_project(callback) {
        this.show_dialbox({
            title: t("You have unsaved changes"),
            render_object: {
                tag: "p", class: "warn-message", contents: t("Are you sure you want to exit the project?")
            },
            on_cancel: this.hide_dialbox.bind(this),
            on_submit: () => {
                callback({ ignore_unsaved: true });
            },
            submit_button_text: t("Ignore the changes"),
            submit_button_icon: icon_exit,
            add_actions: [{
                text: t("Export the changes and exit"),
                icon: icon_export,
                onclick: () => {
                    callback({ save_and_exit: true });
                },
            }]
        })
    }

    /**
     * This is called on any change made to the project data.
     */
    set_saved(value) {
        this.state.saved = value;
        this.update_title_unsaved_tip();
    }

    /**
     * Opens a dialog box with some parameters, depending of the content of the dialog box.
     * @param {Object} params 
     */
    show_dialbox(params) {
        if (!this.state.register_dialbox) {
            this.state.register_dialbox = new DialBox(params);
            obj2htm.subRender(this.render_dialbox(), document.getElementById("project-view"), { mode: "append" });
        } else {
            this.state.register_dialbox.clear_errors();
            this.state.register_dialbox.params = params;
            this.state.register_dialbox.refresh();
        }
    }

    /**
     * Closes the dialog box, clean up event listeners
     */
    hide_dialbox() {
        this.state.register_dialbox.remove_cancel_listeners();
        this.kill_dialbox();
        obj2htm.subRender({ tag: "div" }, document.getElementById("dial-box-modal"), { mode: "remove" })
    }

    /**
     * Deletes the registered dialbox instance if it exists
     */
    kill_dialbox() {
        if (this.state.register_dialbox) {
            delete this.state.register_dialbox;
        }
    }

    /**
     * Get the render object of the dialbox
     * @returns {Object} the object representation of the html for the dialbox component
     */
    render_dialbox() {
        return {
            tag: "div",
            id: "dial-box-modal",
            contents: [this.state.register_dialbox.render()]
        };
    }

    /**
     * Refreshes the rendering of the action_panel component
     */
    refresh_action_panel() {
        this.action_panel.refresh();
    }

    /**
     * Navigates to the game player screen with the edited project data
     */
    test_play_project() {
        this.params.navigate("GamePlayer", {
            project: this.project,
            config: this.config,
            from_app: true,
            app_state: this.state
        });
    }

    /**
     * A callback called when the editor gets closed.
     * Handles the state.saved flag.
     * @param {Object} options 
     */
    on_exit_project(options = { ignore_unsaved: false, save_and_exit: false }) {
        if (this.state.saved || options.ignore_unsaved) {
            app.exit();
        } else if (options.save_and_exit || options.on_saved) {
            this.save_project(options.save_and_exit ? { exit: true } : { on_saved: options.on_saved });
        } else {
            this.on_exit_unsaved_project(this.on_exit_project.bind(this));
        }
    }

    /**
     * Callback closed when a new file is loaded into the editor
     * @param {Object} data A deserialized project
     * @param {Object} options 
     */
    on_load_project_data(data, options = { ignore_unsaved: false, save_and_exit: false }) {
        if (this.state.saved || options.ignore_unsaved) {
            this.project.load_data(data);
            this.project_view.refresh();
            this.action_panel.refresh();
        } else if (options.save_and_exit) {
            this.save_project({ load_data: data });
        } else {
            this.on_exit_unsaved_project(this.on_load_project_data.bind(this, data));
        }
    }

    /**
     * Refreshes the rendering on individual children components project_view, action_panl and menu_panel
     */
    refresh(hard = false) {
        if (hard) {
            obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
        } else {
            this.project_view.refresh();
            this.action_panel.refresh();
        }
    }

    /**
     * Shows a full window loader with the needed options like message etc.
     * @param {Object} options 
     */
    show_loading_state(options) {
        obj2htm.subRender(new LoaderModal().render(options), document.getElementById(this.id));
    }

    /**
     * Closes the loader modal
     */
    hide_loading_state() {
        const modal = document.getElementsByClassName("mtlo-loader-modal")[0];
        modal && modal.remove();
    }

    /**
     * @returns {Object} Object description of the html element for this component
     */
    render() {
        const { project, config } = this;

        this.project_view = new ProjectView({
            project,
            config,
            set_saved: this.set_saved.bind(this),
            show_dialbox: this.show_dialbox.bind(this),
            hide_dialbox: this.hide_dialbox.bind(this),
            kill_dialbox: this.kill_dialbox.bind(this),
            refresh_action_panel: this.refresh_action_panel.bind(this),
        });

        this.action_panel = new ActionPanel({
            project,
            project_view: this.project_view,
            open_project: this.open_project.bind(this),
            set_saved: this.set_saved.bind(this),
            test_play_project: this.test_play_project.bind(this),
            on_save_project: this.save_project.bind(this),
            on_exit_project: this.on_exit_project.bind(this),
        });

        return {
            tag: "div",
            id: this.id,
            on_render: this.handle_open_with_file.bind(this),
            contents: [
                this.action_panel.render(),
                this.project_view.render(),
            ],
        }
    }
}

module.exports = App