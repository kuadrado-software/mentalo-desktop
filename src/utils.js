"use strict";

function file_has_ext(file, ext) {
    return file.name.split(".").includes(ext)
}

module.exports = {
    file_has_ext
}