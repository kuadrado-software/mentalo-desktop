"use strict";

const { app_config_file_path } = require("./constants");
const fs = require("fs");
const path = require("path");


const default_data = {
    prefered_language: "en",
    supported_languages: ["en", "fr", "es"],
    recently_opened_files: [],
};

function create_file(pth, content) {
    const d_path = path.dirname(pth);
    if (!fs.existsSync(d_path)) {
        fs.mkdirSync(d_path, { recursive: true })
    }

    fs.writeFileSync(pth, content);
}

const MAX_RECENTLY_OPEN_FILES = 10;

class Config {
    constructor() {
        if (!fs.existsSync(app_config_file_path)) {
            create_file(app_config_file_path, JSON.stringify(default_data));
        }

        const config = JSON.parse(fs.readFileSync(app_config_file_path));

        this.data = default_data;

        Object.entries(config).forEach(item => {
            const [k, v] = item;
            this.data[k] = v;
        });
    }

    set_entry(key, value) {
        this.data[key] = value;
        this.write_config();
    }

    get_entry(key) {
        return this.data[key];
    }

    add_recently_opened_file(path) {
        this.data.recently_opened_files.push(path);
        const updated = Array.from(new Set(this.data.recently_opened_files));
        this.data.recently_opened_files = updated.slice(updated.length - MAX_RECENTLY_OPEN_FILES);
        this.write_config();
    }

    remove_recently_open_file(path) {
        this.data.recently_opened_files.splice(this.data.recently_opened_files.indexOf(path), 1);
    }

    write_config() {
        fs.truncateSync(app_config_file_path, 0);
        fs.writeFileSync(app_config_file_path, JSON.stringify(this.data));
    }
}


module.exports = Config;