module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "39.257874mm",
    height: "36.491329mm",
    viewBox: "0 0 39.257873 36.491329",
    version: "1.1",
    class: "svg-icon icon-check",
    contents: [
        {
            tag: "g", transform: "translate(-64.326825,-109.15347)", contents: [
                {
                    tag: "path",
                    d: "m 82.619549,145.6448 c -1.791321,-4.56932 -10.621635,-14.06205 -18.292724,-17.9618 6.815792,1.61913 13.082117,4.12277 16.789158,5.73045 0,0 20.033947,-22.41445 22.468717,-24.25998 -6.025204,9.16337 -19.434924,33.63534 -20.965151,36.49133 z"
                }
            ]
        },
    ]
}