module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "42.248665mm",
    height: "42.248665mm",
    viewBox: "0 0 42.248665 42.248665",
    version: "1.1",
    class: "svg-icon icon-white-sheet",
    contents: [
        {
            tag: "g", transform: "translate(-85.464951,91.427902)", contents: [
                {
                    tag: "path",
                    d: "m 91.676737,-49.179237 v -27.262666 l 14.985993,-14.985999 h 15.15534 v 42.248665 z m 27.855333,-2.286 v -37.676665 h -10.58334 v 12.869333 H 93.944774 c 0,8.259376 0.01796,17.365981 0.01796,24.807339 8.523113,-7e-6 17.046223,-7e-6 25.569333,-7e-6 z m -12.37325,-26.455497 0.14174,-11.689292 -11.617465,11.666228 c 0,0 8.611175,0.01538 11.475725,0.02306 z"
                }
            ]
        },
    ]
};