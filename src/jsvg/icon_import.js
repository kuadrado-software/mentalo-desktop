module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "38.946663mm",
    height: "42.333332mm",
    viewBox: "0 0 38.946663 42.333332",
    version: "1.1",
    class: "svg-icon icon-import",
    contents: [
        {
            tag: "g", transform: "translate(127.08061,15.250039)", contents: [
                {
                    tag: "path",
                    d: "M -118.44461,27.083293 V 11.589294 h 5.16467 v 10.329333 h 19.981325 V -10.085372 H -113.27994 V 0.24396136 h -5.16467 V -15.250039 h 30.310662 v 42.333332 z m 13.54666,-13.335689 v -5.20631 h -22.18266 V 3.3766274 h 22.18022 l 0.0448,-5.165465 7.746999,7.748485 z"
                }
            ]
        },

    ]
}