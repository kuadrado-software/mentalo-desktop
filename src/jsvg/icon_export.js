module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "42.079334mm",
    height: "42.333328mm",
    viewBox: "0 0 42.079334 42.333328",
    version: "1.1",
    class: "svg-icon icon-export",
    contents: [
        {
            tag: "g", transform: "translate(-46.240093,-85.42262)", contents: [
                {
                    tag: "path",
                    d: "m 58.008762,127.75595 v -15.494 h 5.164666 v 10.32934 H 83.15476 V 90.587286 H 63.173428 v 10.329334 h -5.164666 v -15.494 h 30.310664 v 42.33333 z m -4.021666,-13.37653 -7.747003,-7.74849 7.789337,-7.787954 v 5.206314 h 22.182665 v 5.16466 H 54.031877 Z"
                }
            ]
        },

    ]
};