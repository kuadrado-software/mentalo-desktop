"use strict";

const path = require("path");
const os = require("os");

const root_path = path.dirname(__dirname);

module.exports = {
    custom_project_mime: "mtl",
    ui_config: {
        project_view: {
            animation_canvas_frame_rate: 30,
            padding: 50
        }
    },
    translations_path: path.join(root_path, "assets", "translations"),
    app_config_file_path: path.join(os.homedir(), ".config", "mentalo", "config.json"),
    images_path: path.join(root_path, "assets", "images"),
    demo_game_assets_path: path.join(root_path, "assets", "demo_game_assets", "home_sweet_home"),
    import_image_supported_mimes: ["png", "jpg", "bmp", "jpeg"],
    supported_locales: ["fr", "en", "es"]
}