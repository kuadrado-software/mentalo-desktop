"use strict";
const path = require("path");
const fs = require("fs");
const { translations_path } = require("./constants");

class Translator {
    constructor(config) {
        this.locale = config.get_entry("prefered_language") || "en";
        this.supported_languages = config.get_entry("supported_languages") || ["en"];
        this.translations_path = this.get_translations_path();
        this.translations = this.get_translations();
        global.t = this.trad.bind(this);
    }

    change_locale(locale) {
        if (!this.supported_languages.includes(locale)) {
            console.error("unsupported locale", locale);
            return;
        }
        this.locale = locale;
        this.translations_path = this.get_translations_path();
        this.translations = this.get_translations();
    }

    get_translations_path() {
        return path.join(translations_path, this.locale + ".json");
    }

    get_translations() {
        return JSON.parse(fs.readFileSync(this.translations_path))
    }

    trad(text, params = {}) {
        text = this.translations[text] || text;

        Object.keys(params).forEach(k => {
            text = text.replace(`{%${k}%}`, params[k]);
        });

        return text;
    }
}

module.exports = Translator;