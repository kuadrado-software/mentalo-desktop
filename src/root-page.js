"use strict";

const ToolTipsManager = require("tooltips-manager");
const LoaderModal = require("./loader-modal");


class Loading {
    render() {
        return new LoaderModal().render()
    }
}

const SCREENS = {
    App: require("./app/app"),
    GamePlayer: require("./game-player"),
    Loading,
};

/**
 * Render the main element at the root of the app.
 */
class RootPage {
    constructor() {
        this.state = {
            screen: { name: "App", params: { navigate: this.navigate.bind(this) } }
        };

        this.tooltips_manager = new ToolTipsManager({
            style: { backgroundColor: "#222d", zIndex: 101 },
            pop_tooltip_delay: 200,
        });

    }

    /**
     * Handles screen change.
     * @param {String} screen_name The constructor name of the wanted screen
     * @param {Object} params A litteral object that will be passed to the new screen instance as parameter
     */
    navigate(screen_name, params = {}) {
        params = Object.assign(params, { navigate: this.navigate.bind(this) })
        this.state.screen = { name: screen_name, params };
        obj2htm.renderCycle();
    }

    /**
     * Build the rendering object for this component.
     * The screen is rendered as a child node, it's selected by name dynamically from state.
     * If a method clear() exists on the rendered screen instance it will be called.
     * @returns {Object} Object representation of the html element
     */
    render() {
        if (this.register_screen
            && this.register_screen.clear
            && typeof this.register_screen.clear === "function") {
            this.register_screen.clear();
        }

        this.register_screen = new SCREENS[this.state.screen.name](this.state.screen.params);

        return {
            tag: "main",
            contents: [
                this.register_screen.render(),
            ],
        };
    }
}

module.exports = RootPage;
